# Description of communication via postMessage

### Legend
- **REC**: means game receives a message
- **SEND**: means game sends a message
- `string` string message

### Cross-Site-Request-Forgery
All request to the backend needs to send a header with an csrf tocken, e.g.

```
    X-CSRF-TOKEN:7e2e815f-acd3-4eab-abd6-de727466ee98
```

The value must be taken from the same named cookie:

```
    X-CSRF-TOKEN
```


### Messages flow 'normal game'
1. Page with a game has been loaded.
1. SEND

```json
  {
    "command": "gameLoaded",
    "game": "bingo",
    "element": "",
    "action": ""
  }
```

1. Customer clicks **Play** button.
1. REC `playGame`
1. Game `HTTP POST <url>/product/iwg/bingo/tickets <- JSON` with ticket description and scenario object.

Besides ticket description and scenario object the returned ticket contains three ids also:

```json
  {
      "id": "<id of the ticket which must be used to settle it later>",
      "externalId": "<id of the ticket in pool>",
      "ticketNumber": "<number of the ticket seen by a customer>",
      "..."
  }
```

Real example:
```json
   {
      "bundleKey": "376594308",
      "customerNumber": "95384613",
      "externalId": "cashbustertowers:v1:2:pool:0:ticket:1577002",
      "game": {
        "active": true,
        "canonicalName": "cashbustertowers",
        "displayName": "Cash Buster Towers",
        "labels": {
          "type": "regular"
        },
        "newGame": true,
        "restricted": false,
        "visibleDesktop": true,
        "visibleMobile": true
      },
      "id": 376594308,
      "played": false,
      "price": {
        "amount": "5.00",
        "currency": "EUR",
        "amountInMinor": 500,
        "amountInMajor": 5
      },
      "scenario": "eyJpZCI6MTAxLCJzY2VuYXJpbyI6IntcInRpZXJcIjowLFwiaWRcIjoxMDEsXCJyb2xsc1wiOjUsXCJ0dXJuRGF0YVwiOltbXCJGXCIsNCxcIkJcIiwzLDIsMV0sWzMsMixcIk1cIixcIk1cIiwzLDRdLFsxLDMsMiwyLDQsMl0sWzEsMSw0LDEsMSw1XSxbMSxcIlRcIiwzLDUsMixcIk1cIl0sW1wiVFwiLDEsMiwyLFwiVFwiLDNdXSxcImJvbnVzQ2FyZEdhbWVEYXRhXCI6W10sXCJib251c1doZWVsR2FtZURhdGFcIjpbXSxcInNjZW5hcmlvSW5mb1wiOntcInN5bWJvbEtleVwiOntcIm5vV2luXCI6XCJOXCIsXCJleHRyYVNwaW5cIjpcIkZcIixcInJlc2V0VG93ZXJcIjpcIlRcIixcImluc3RhbnRXaW5cIjpcIklcIixcImJvbnVzV2luXCI6XCJCXCIsXCJib251c0dhbWVcIjpcIk1cIixcImJsb2Nrc1wiOlwiQlwiLFwiYmxhbmtcIjpcIlhcIn0sXCJsYXlvdXRcIjp7XCJ0b3dlckhlaWdodHNcIjpbMTEsMTIsMTMsMTQsMTUsMTZdfSxcInByaXplc1wiOntcInRvd2VyUHJpemVzXCI6WzEsNCwxMSwxNSwxNywxOF0sXCJpbnN0YW50UHJpemVzXCI6WzIsOV0sXCJhbGxJbnN0YW50UHJpemVzXCI6WzIsOV0sXCJib251c1ByaXplXCI6W1wiXCIsXCJcIixcIlwiLDUsMTMsMTZdfSxcIm91dGNvbWVcIjp7XCJ0b3dlcldpbnNcIjpbXSxcImluc3RhbnRXaW5zXCI6W10sXCJib251c1dpblwiOi0xfX19In0=",
      "soldAt": 1496860215.565,
      "ticketNumber": "6910608",
      "winningClass": {
        "number": 0,
        "numberOfTickets": -1,
        "winnings": {
          "amount": "0.00",
          "currency": "EUR",
          "amountInMinor": 0,
          "amountInMajor": 0
        },
        "winningsType": "NoWinning"
      }
    }
```
1. SEND

```json
    {
        "command": "gameStarted",
        "ticketId": "<ticket id from JSON ticket (field name 'ticketId')>",
        "externalId": "<external id from JSON ticket (field name 'externalId')>",
        "ticketNumber": "<ticket number from JSON ticket (field name 'ticketNumber')>",
        "game": "bingo",
        "element": "",
        "action": ""
    }
```

1. Game `HTTP POST <url>/product/iwg/bingo/tickets/{id}/settle <- 200`
   The id is the 'id' from the above mentioned ticket purchase response.
1. SEND

```json
    {
        "command": "ticketSettled",
        "game": "bingo",
        "element": "",
        "action": ""
    }
```

1. Customer clicks **Weiter** button on popup.
1. SEND

```json
    {
        "command": "gameFinished",
        "game": "bingo",
        "element": "go_on",
        "action": "click"
    }
```

### Messages flow 'demo game'
1. Page with a game has been loaded.
1. SEND

```json
    {
        "command": "gameStarted",
        "game": "bingo",
        "ticketId": "<ticket id from JSON ticket (field name 'ticketId')>",
        "externalId": "<external id from JSON ticket (field name 'externalId')>",
        "ticketNumber": "<ticket number from JSON ticket (field name 'ticketNumber')>",
        "element": "",
        "action": ""
    }
```

1. Customer clicks **Play Demo** button.
1. REC `playDemoGame`.
1. Game `HTTP POST <url>/product/iwg/bingo/demoticket <- JSON` with tickets description and scenario object.
1. SEND

```json
    {
        "command": "gameStarted",
        "game": "bingo",
        "ticketId": "<ticket id from JSON ticket (field name 'ticketId')>",
        "externalId": "<external id from JSON ticket (field name 'externalId')>",
        "ticketNumber": "<ticket number from JSON ticket (field name 'ticketNumber')>",
        "element": "",
        "action": ""
    }
```

1. Game `HTTP POST <url>/product/iwg/bingo/tickets/-1/settle <- 200`
1. Customer clicks **Weiter** button on popup.
1. SEND

```json
    {
        "command": "gameFinished",
        "game": "bingo",
        "element": "go_on",
        "action": "click"
    }
```

### Messages flow 'in purchase game'

This subsection provides additional information for a game where the purchase of a ticket or demo ticket happens from inside the game and not the webshop. This can be the case when a game offers a rebuy ticket functionality or different bet stakes.

* `gameLoaded` command adjustment. A game that offers "in game purchase" must sent additional field with `gameLoaded` command. The field is named `inGamePurchase` and is of `boolean` type. It is optional also.
```json
  {
    "command": "gameLoaded",
    "game": "slingo",
    "inGamePurchase": true,
    "element": "",
    "action": ""
  }
```
* New `prepareGame` command. When a customer starts the game instead of `playGame` or `playDemoGame` a new command will be sent to the game:
```json
{
  "command": "prepareGame",
  "demo": "true or false"
}
```

The `demo` field indicates whether a game should prepare demo game or normal one.

The game should react by hiding the loader screen and start (moving to choose bet stake screen for example).

* Once a customer chose a bet stake the game must sent a new event to the webshop:
```json
{
  "command": "buy",
  "betFactor": 1.5
}
```
Once the webshop receives this event it will validate if a customer can buy such a ticket and if yes it will sent the usual `playGame` command back to the game. The game can then purchase the desired ticket with the usual URL extended by new `betFactor` parameter `HTTP POST <url>/product/iwg/bingo/tickets?betFactor=1.5`. This same applies to demo ticket `HTTP POST <url>/product/iwg/bingo/demoticket?betFactor=1.5`

### Rebuy

When a rebuy of a ticket should be performed from inside a game that doesn't offer different ticket stakes then the `buy` command without `betFactor` must be sent to webshop.
```json
{
  "command": "buy",
  "game": "mojimoney"
}
```
This will perform all the necessary checks (customer account balance etc...) and post the `playGame` message back to the game as usual.

### Messages flow 'click tracking'
- Customer clicks **Start** button on the game.
    - SEND

```json
    {
        "command": "startClicked",
        "game": "bingo",
        "element": "play_game",
        "action": "click"
    }
```

- Customer clicks **Sound on** button on the game.
    - SEND

```json
    {
        "command": "soundOnClicked",
        "game": "bingo",
        "element": "sound_on",
        "action": "click"
    }
```

- Customer clicks **Sound off** button on the game.
    - SEND

```json
    {
        "command": "soundOffClicked",
        "game": "bingo",
        "element": "sound_off",
        "action": "click"
    }
```

- Customer clicks **Turbo** button on the game.
    - SEND

```json
    {
        "command": "turboClicked",
        "game": "bingo",
        "element": "turbo",
        "action": "click"
    }
```

- User clicks 'weiter' button on the game.
    - already cover on last step of 2 previous sections (gameFinished).

### Custom game specific tracking

Normally above mentioned tracking commands must also be registered in the alinghi webhsop source code. To bypass this, a custom tracking command has been introduced. Sending custom tracking looks like this:
  - SEND

```json
    {
        "command": "track",
        "game": "dickehose",
        "element": "open_all_button",
        "action": "click"
    }
```

```json
    {
        "command": "track",
        "game": "foo_bar",
        "element": "gun",
        "action": "fire"
    }
```

Every custom tracking command is called *track*. The old tracking commands are left for backward compatibility.

### Commands
- `gameLoaded`
	- Sends from game after it has been loaded.
- `playGame`
	- Sends from platfrom to game after customers has clicked the 'Play game' button in order to start playing.
- `playDemoGame`
	- Sends from platfrom to game after customers has clicked the 'Play Demo' button in order to start playing a demo game.
- `gameStarted`
	- Sends from game to indicate that the game will have been started.
- `ticketSettled`
	- Sends from game after succesful invocation of `HTTP POST <url>/product/iwg/bingo/tickets/{ticketId}/settle`
- `gameFinished`
	- Sends from game to indicate that the game has been finished when customer clicks **Weiter** button.
- `startClicked`
    - Sends from game when customer clicks **Start** button on the game.
- `soundOnClicked`
    - Sends from game when customer clicks **Sound On** button on the game.
- `soundOffClicked`
    - Sends from game when customer clicks **Sound Off** button on the game.
- `turboClicked`
    - Sends from game when customer clicks **Turbo** button on the game.
- `JSON error object` see **Error Handling** below.
	- Sends from game when some error occurs.

## Error Handling
Each error that occurs inside the game can wrapped in a JSON error object and posted via postMessage.

### JSON error object schema

```json
	{
		"type":"object",
		"properties":{
			"type": {
				"type":"string",
				"required":true
			},
			"title": {
				"type":"string",
				"required":false
			},
			"status": {
				"type":"number",
				"required":false
			},
			"details": {
				"type":"string",
				"required":false
			},
			"reloadGame": {
				"type":"boolean",
				"required":false
			}
		}
	}
```

### Example

```json
	{
	    "type": "urn:x-tipp24:remote-error",
	    "title": "Remote Error",
	    "status": 403,
	    "details": "Operation Forbidden",
	    "reloadGame": true
	}
```

### Possible error types
- `urn:x-tipp24:remote-client-error`
	- An error ocurred on the client side (game) during XHR processing. Game is a client for Alinghi web shop in this case.
- `urn:x-tipp24:remote-error`
	- Error during XHR processing. Session timed out for example. Used in Alinghi webshop.
- `urn:x-tipp24:post-message-error`
	- Error during sending or receiving a message via postMessage interface.
- `urn:x-tipp24:error`
	- Any other kind of error.
