# ZIG game frontend

This document describes how to use `zig-client` and structure frontend code.

## Note

Game has to be scalable. The loader image should scale itself also.

## Obligatory files

A game must have two files with sctrict names in the root folder: `inner.html`, `outer.html`.

### Example structure

```
<game-name>/
|-- <...game resources...>
|-- inner.html
`-- outer.html
```

### inner.html

The `inner.html` is just like an `index.html` for the game. It may contains any content required by the game.

### outer.html

The content of this file is almost always fixed and looks like this:

```html
<!DOCTYPE html>
<html>
<body>
<script>
    window.GameSettings = {"aspect": 1.329787, "index": "inner.html"};
</script>
<script src="https://lib.zig.services/zig/1-stable/wrapper.js"></script>
</body>
</html>
```

# ZIG client

ZIG client is available as `node` module. If your frontend is built with node you can add the dependency to your project with `npm install --save zig-js`. In other case include `libzig.js` script in `inner.html` like this:

```html
<script src="https://lib.zig.services/zig/1-stable/libzig.js"></script>
```

This script exposes one JavaScript **object** `window.Zig.Client`.

This object contains the following API:

```javascript
async buyTicket(payload: any = {}, options: BuyTicketOptions = {}): Promise<ITicket>;
async demoTicket(payload: any = {}, options: BuyTicketOptions = {}): Promise<ITicket>;
async settleTicket(id: string): Promise<void>;
```

With `BuyTicketOptions` object a bet factor may be sent for example:

```JavaScript
window.Zig.Client.buyTicket(null, { betFactor: 2 })
  .then(ticketResponse => startGame(ticketResonse))
  .reject(err => /* do something with error if you wish otherwise leave to zig-client. */)
```

## Message Client

`Zig.Client` exposes an interface for message communication with `interface` object => `window.Zig.Client.interface`.

The object contains the following API:
```javascript
send(message: any): void;
sendError(err: any): void;
register<K extends Command>(type: K, handler: (msg: CommandMessageTypes[K]) => void): Unregister
registerGeneric(handler: Partial<CommandMessageHandlers>): Unregister
```

**send** - sends a message to the parent containing the game on its site. The example below informs that a game has been completely loaded.

```javascript
ZigMessageClient.send({
    command: "gameLoaded",
    game: "<game name>"
});
```

**sendError** - sends a message to the parent containing the game on its site in case of error situation on the game side. It can be failure during purchasing a ticket for example. The error object passed over looks like this:

```javascript
export interface IError {
    type: string;
    title: string;
    status?: number; // HTTP status code if available
    details?: string;
}
```

**register** - registers a function for a given event. A game has to be notified when to start purchasing ticket for example. For more information about events see [messaging](https://bitbucket.org/esailorsiwg/iwg-docu/src/036e8b7032e0b0b9eb321b602db836fd1c00095b/iframe-communication-postMessage-api.md). An example of hanlding `playGame` event can be found below:

```javascript
window.Zig.Client.interface.register("playGame", () => {
    window.Zig.Client.buyTicket().then(response => {
      /* do something with ticket response */
      },
      { /* on error */ });
});
```
