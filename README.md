# Basic requirements for an instant win game integration

* Needs to run on all current browsers without plugins (Chrome 45+, Firefox 38+, IE 11+, Safari 9+, Android 4.1+, iOS8+)
* 3MB maximum download size at gamestart
* a maximum of 10 files (please use sprites)
* If the loading takes longer, there needs to be a progress bar (or busy indicator) shown (design will be provided by us)
* The game runs in an iFrame

## API Endpoints

```
/tickets	buys a ticket
/settle		marks the ticket as finished
```

* The ticket contains the selected scenario, a ticket ID and the prize. 


## Communication with the shop through window.postMessage – Format is given. Containing things like:

* <- Game loaded
* -> Start game now
* <- Game played
* <- Game finished completely
* <- Tracking of single events (Button clicked etc.)

A detailed API description can be found here: [IFrame post message communication](iframe-communication-postMessage-api.md)